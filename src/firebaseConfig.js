var admin = require("firebase-admin");

var serviceAccount = require("./react-notifications-8d070-firebase-adminsdk-cg7l8-7295516a7e.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: ""
});

module.exports = admin