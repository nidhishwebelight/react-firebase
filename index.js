const express = require("express")

const admin = require("./src/firebaseConfig")

const app = express()
app.use(express.json())
const port = 3009
const notification_options = {
   priority: "high",
   timeToLive: 60 * 60 * 24,
}
app.post("/sendNotification", (req, res) => {
   const { title, body, registrationToken } = req.body;
   var payload = {
      notification: {
         title,
         body,
      },
   }
   const options = notification_options
   admin
      .messaging()
      .sendToDevice(registrationToken, payload, options)
      .then((response) => {
         res.status(200).send("Notification sent successfully")
      })
      .catch((error) => {
         console.log(error)
         res.send(error)
      })
})
app.listen(port, () => {
   console.log("listening to port: " + port)
})
